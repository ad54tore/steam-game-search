<?php
require_once 'vendor/autoload.php';

use Elasticsearch\ClientBuilder;

$connectionPool = '\Elasticsearch\ConnectionPool\StaticNoPingConncetionPool';
$client = ClientBuilder::create()->build();
