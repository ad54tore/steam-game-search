 <?php
	
require_once 'app/init.php';

//Define 'Baseline' parameters for text-only search
$hits = 0; //Placeholder 

$size = 10; //amount of documents 
$dlccount = 0;
$players = 0;
$mc_score = 0;
$controller = 'true, false';
$releasedAfter = '1970-01-01';
$releasedBefore = '2020-01-01';

$platformquery = '*';

//Calculate platform query 
if(isset($_GET['controller'])) $controller = 'true';

	if(isset($_GET['windows']) ) {
		$platformquery = $platformquery.'windows ';
	}
	if(isset($_GET['linux'])) {
		$platformquery = $platformquery.'linux ';
	}
	if(isset($_GET['mac'])) {
		$platformquery = $platformquery.'mac ';
	}
	//echo $platformquery;
	
//Require atleast something in the text field
if(isset($_GET['q'])) {
	//Override values with either standard values or user input
	$q = $_GET['q'];
	$size = $_GET['size'];
	$players = $_GET['players'];
	$dlccount = $_GET['dlc'];
	$mc_score = $_GET['mc_score'];
	$releasedAfter = $_GET['releasedAfter'];
	$releasedBefore = $_GET['releasedBefore'];
	//remove "game" and "games" and "with" as they're really unneccessary
	$q0 = strtolower($q);
	$q1 = str_replace('games', '', $q0);
	$q2 = str_replace('game', '', $q1);
	$q3 = str_replace('with', '', $q2);
	$q4 = str_replace('to', '', $q3);
	$q5 = str_replace('play', '', $q4);
	//remove leading, trailing and extra whitespaces
	$q6 = trim(preg_replace('/\s+/', ' ', $q5));
	//remove special characters except "-" and " "
	$qFinal = trim(preg_replace('/[^\w\s-]/', '', $q6));
	//echo '_'.$qFinal.'_';

	//Construct query
		$params = [
			'index' => 'games',
			'type' => '_doc',
			'size' => $size,
			'body' => [
				'query' => [
					'function_score' => [
						'query' => [ 
							'bool' => [
								'should' => [
									//Tests if the user search text is in documents name or abouttext (text) with slight errors (fuzziness aka Levenshtein distance) and wildcards
									[ 'wildcard' => [ 'name' =>  ['value' => '*'.$qFinal.'*', 'boost' => 1.0				 ]] ],
									[ 'wildcard' => [ 'text' =>  ['value' => '*'.$qFinal.'*', 'boost' => 2.0				 ]] ],
									[ 'match' => 	['text' => 	 ['query' => $qFinal, 'fuzziness' => '1', 'boost' => 2.0  ]]	],
									[ 'wildcard' => [ 'genres' =>  ['value' => '*'.$qFinal.'*', 'boost' => 2.0				 ]] ],
									[ 'match' => 	['genres' => 	 ['query' => $qFinal, 'fuzziness' => '1', 'boost' => 2.0  ]]	]
								],
								'must' => [
								//Games have to support controllers and platforms if user enters them
									[ 'match' => 	['controller' => 	['query' => $controller ]] ],
									[ 'query_string' => [ 'query' => $platformquery, 'fields' => ['platforms']] ]
								],
								'filter' => [ 
								//filters resultset based on more paramaters, doesnt  influence scoring
									[ 'range' => [ 'players' => 	[ 'gte' => $players			] ] ],
									[ 'range' => [ 'dlc' => 		[ 'gte' => $dlccount		] ] ],
									[ 'range' => [ 'mc_score' => 	[ 'gte' => $mc_score		] ] ],
									[ 'range' => [ 'releasedate' => [ 'gte' => $releasedAfter	] ] ],
									[ 'range' => [ 'releasedate' => [ 'lte' => $releasedBefore	] ] ]
								],
								'minimum_should_match' => '50%' //atleast half of the users query has to appear atleast once in document
							]
						],
						'field_value_factor' => [
						//increases score by metacritic score, so that "better" games appear more often
							'field' => 'mc_score',
							'modifier' => 'none',
							'factor' => 1.0,
							'missing' => 50
						]
					]
				],
				'highlight' => [ 
				//highlights keywords in the abouttext section if they appear
					'pre_tags' => ['<strong>'], 
					'post_tags' => ['</strong>'],
					'fields' => [ 'text' => new \stdClass(), 'name' => new \stdClass()	],
					'require_field_match' => true,
					'fragment_size' => 255
				]/*,
				'min_score' => 0.5*/
				//prevents irrelevant documents from showing up
			]
		];

	//start the search
	$query = $client->search($params);
	
	//expect more than 0 results
	if($query['hits']['total'] >= 1) {
		$results = $query['hits']['hits'];
	}
	
	$totalHits = count($query['hits']['total']);
	$hits = count($query['hits']['hits']);
}

?>
<!doctype html>
<html>
	<head>
		<meta charset="utf8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="css/style.css">
		<script type="text/javascript" src="js/script.js"></script>
		<!Toggle dark mode>
		<script>
			document.addEventListener('DOMContentLoaded', (event) => {
				((localStorage.getItem('mode') || 'dark') === 'dark') ? document.querySelector('body').classList.add('dark') : document.querySelector('body').classList.remove('dark')
			});
			
			
			</script>
		<title>Steam Game Search</title>
	</head>
	<body>
		<header>	
			<!Add a stored dark mode>
			<p id="lightSwitch" class="noselect" href="#" onclick="
			  localStorage.setItem('mode', (localStorage.getItem('mode') || 'dark') === 'dark' ? 'light' : 'dark'); 
			  localStorage.getItem('mode') === 'dark' ? document.querySelector('body').classList.add('dark') : document.querySelector('body').classList.remove('dark');" title="Dark/light">&#x1F313 	
			</p>
		</header>
		<br>
		<div class="searchbar <?php if(isset($results)) echo 'display';?>">
			<p class="logo">Steam Game Search</p>	
			
			<!Define text query here>
			<form action="index.php" method="get" autocomplete="off">
				<input type="text" name="q" size="64" value="<?php if(isset($_GET['q'])) { echo htmlentities($_GET['q']); } ?>"/>
				<input type="submit" value="Go!" width="20px" height="20px"></br>
				
				<!Extend query here>
				<div id="formdivider" style="display:none">
					<div>Number of Results: <input type="number" name="size" min="0" max="100" value="<?php if(isset($_GET['size'])) { echo htmlentities($_GET['size']); } else echo '20'?>" /></div>
					<div>Min number of players: <input type="number" name="players" min="0" value="<?php if(isset($_GET['players'])) { echo htmlentities($_GET['players']); } else echo '0';?>" /></div>
					<div>Min number of DLC: <input type="number" name="dlc" min="0" max="100" value="<?php if(isset($_GET['dlc'])) { echo htmlentities($_GET['dlc']); } else echo '0';?>" /></div>
					<div>Min MetaCritic Score: <input type="number" name="mc_score" min="0" max="100" value="<?php if(isset($_GET['mc_score'])) { echo htmlentities($_GET['mc_score']); } else echo '0'?>" /></div>
					<div id="booleanForm">
						<div>Windows <input type="checkbox" name="windows" value="True"<?php if(isset($_GET['windows'])) echo "checked='checked'"; ?>></div>
						<div>Linux <input type="checkbox" name="linux" value="True"<?php if(isset($_GET['linux'])) echo "checked='checked'"; ?>></div>
						<div>Mac <input type="checkbox" name="mac" value="True"<?php if(isset($_GET['mac'])) echo "checked='checked'"; ?>></div>
						<div>Must Support controllers <input type="checkbox" name="controller" value="True"<?php if(isset($_GET['controller'])) echo "checked='checked'"; ?>></div>
					</div>
					<div>Released after <input type="date" name="releasedAfter" value="1970-01-01" min="1970-01-01" max="2020-01-01"></div>
					<div>Released before <input type="date" name="releasedBefore" value="2020-01-01" min="1971-01-01" max="2020-01-01"></div>
				</div>
			</form>
			<!Open the "More Paramater" Box>
			<button id="extendMore" value="Extended parameters" onclick="
															var formdiv = document.getElementById('formdivider');
															var extendMore = document.getElementById('extendMore');
															if(formdiv.style.display === 'none') { 
																formdiv.style.display = ''; 
																extendMore.innerHTML='Less';
															} else {
																formdiv.style.display = 'none'; 
																extendMore.innerHTML='More';
															};">More</button>
		</div>
		
		<div class="resultList" <?php echo (!isset($results)) ? "style='display: none'" : "style='display:show'"; ?> >
		<?php if($hits == 0 ) { echo "<p class='error'>Sorry, but we didn't find anything that fits your search query :( </p>"; } 
			
			//Render each result from search query
			foreach($results as $r) {
			?>
				<a class="result" href="https://store.steampowered.com/app/<?php echo $r['_id']?>" target="_blank"> 
					<img src="https://steamcdn-a.akamaihd.net/steam/apps/<?php echo $r['_id']?>/header.jpg" alt="Header Image" width="460" height="215"> </img>
					<div class="resultText">
						<div class="resultMain">
							<div class="title" href="#<?php echo $r['_id'];?>"><?php echo $r['_source']['name'];?></div>
							<div class="iconRow"><img src="media/windows.png" alt="windows" style="<?php if(!isset($r['_source']['platforms'][0])) echo 'display: none';?>"> </img>
												 <img src="media/linux.png" alt="linux" style="<?php if(!isset($r['_source']['platforms'][1])) echo 'display: none';?>"> </img>
												 <img src="media/mac.png" alt="mac" style="<?php if(!isset($r['_source']['platforms'][2])) echo 'display: none';?>"> </img>
												 <img src="media/controller.png" alt="controller" style="<?php if(strcmp($r['_source']['controller'], 'True')) echo 'display: none;';?>"> </img>
												 <div class="numberImages"><img src="media/metacritic.png" alt="metacritic" id="metacritic"></img><?php echo $r['_source']['mc_score']?></div>
												 <div class="numberImages"><img src="media/dlc.png" alt="dlc"></img><?php echo $r['_source']['dlc']?></div>
												 <div class="numberImages"><img src="media/players.png" alt="players"></img><?php echo $r['_source']['players']?></div>
												 <div class="numberImages"><img src="media/release.png" alt="metacritic"></img><?php echo $r['_source']['releasedate']?></div>
							</div>
							
						</div>
						<div class="description"><?php if(isset($r['highlight']['text'][0])) {
															echo $r['highlight']['text'][0];
														} else { 
															echo substr($r['_source']['text'], 0, 255);
														}?></div>
						<div class="genreList">
							<?php 
							//Render each genre-box
							foreach($r['_source']['genres'] as $genre) { echo "<div class='genre ", $genre, "'>", $genre," </div>"; } 
							?>
						</div>
					<br>
					</div>
				</a>
			<?php
			}
		?>
		</div>
	</body>
</html>