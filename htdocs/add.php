<?php
require_once 'app/init.php';

if(!empty($_POST)) {
	if(isset($_POST['name'], $_POST['genres'], $_POST['text'])) {
		$appID = $_POST['appID'];
		$name = $_POST['name'];
		$genres = explode(',', $_POST['genres']);
		$text = $_POST['text'];
		$releaseDate = $_POST['releaseDate'];
		$age = $_POST['age'];
		$mc_score = $_POST['mc_score'];
		$categories = explode(',',$_POST['category']);
		$lang = explode(',',$_POST['lang']);
		$dlc = $_POST['dlc'];
		$rcmd = $_POST['rcmd'];
		$platforms = explode(',',$_POST['platforms']);
		 
		//erstelle neuen index 
		$indexed = $client->index([
			'index' => 'games',
			'type' => '_doc',
			'id' => $appID,
			'body' => [
				'name' => $name,
				'genres' => $genres,
				'text' => $text,
				'releaseDate' => $releaseDate,
				'age' => $age,
				'mc_score' => $mc_score,
				'categories' => $categories,
				'language' => $lang,
				'dlccount' => $dlc,
				'recommendationcount' => $rcmd,
				'platforms' => $platforms
				
			]
		]);
		//teste ob korrekt indexiert
		if($indexed) {
			print_r($indexed);
		}
	}
}
	
?>

<!doctype html>
<html>
	<head>
		<meta charset="utf8">
		<link rel="stylesheet" href="../css/style.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Add new entry to database</title>
	</head>
	<body>
		<header>
			<a href="index.php">Back</a>
		</header>
		<h1>Add new Entry</h1>
		<form action="add.php" method="post" autocomplete="off">
			<label>ResponseID<br>
				<input type="text" name="appID">
			</label><br>
			<label>Name<br>
				<input type="text" name="name">
			</label><br>
			<label>Genres (Commalist)<br>
				<input type="text" name="genres">
			</label><br>
			<label>About Text<br>
				<textarea name="text" rows="8"></textarea>
			</label><br>
			<label>ReleaseDate<br>
				<input type="text" name="releaseDate">
			</label><br>
			<label>RequiredAge<br>
				<input type="text" name="age">
			</label><br>
			<label>MetaCritic<br>
				<input type="text" name="mc_score">
			</label><br>
			<label>Category (Commalist)<br>
				<input type="text" name="category">
			</label><br>
			<label>Languages(Commalist)<br>
				<input type="text" name="lang">
			</label><br>
			<label>DLC Count<br>
				<input type="text" name="dlc">
			</label><br>
			<label>Recommendation Count<br>
				<input type="text" name="rcmd">
			</label><br>
			<label>Platforms (Commalist)<br>
				<input type="text" name="platforms">
			</label><br>
			<input type="submit" value="add"></input>
		</form>
	</body>
</html>