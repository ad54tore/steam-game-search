# Steam Game Search

Verbesserte Suchmaschine für Steam-Spiele ("Apps").<br>
Unsere Datenmenge basiert auf [diesem Dataset](https://data.world/craigkelly/steam-game-data), jedoch werden nicht relevante Spalten (zB Support Email, Homepage, und andere) entfernt, da diese der Suchmaschine nicht helfen.<br>
Außerdem werden Bool-Type Spalten zu Arrays gefasst, um Genres und Kategorien besser indexieren zu können.<br>
Die Suchmaschine verwendet ElasticSearch 6.7.x als Basis, welches mit Hilfe von Composer für PHP installiert und eingerichtet wurde.<br>

## Installation
### Installiere [XAMPP](https://www.apachefriends.org/index.html)
Folgen Sie dieser [Installationsanleitung](https://vitux.com/how-to-install-xampp-on-your-ubuntu-18-04-lts-system/) um XAMPP zu installieren und somit einen Webserver auf Ihrem Ubuntu aufzusetzen.<br>
Danach können Sie alle Dateien aus dem htcdocs-Ordner in den htcdocs Ordner Ihrer Webservers kopieren bzw. den Gesamtinhalt dieses Gits in den Hauptordner Ihres Webservers kopieren. <br>
Damit sollten Sie sich die folgenden Schritte sparen können. Falls es zu Problemen kommt, folgen Sie einfach der kommenden Anleitung für die manuelle Installation.<br>

### Installiere [ElasticSearch-PHP](https://github.com/elastic/elasticsearch-php) 
Folgen Sie der 'Installation via Composer' Anleitung auf der oben verlinkten github Seite. Ändern Sie im ersten Schritt die Zeile 
```json
"elasticsearch/elasticsearch": "^7.0"
```

zu 

```json
"elasticsearch/elasticsearch": "~6.7"
```

da es sonst zu Kompatibilitätsproblemen kommen kann.

## Konfiguration von ElasticSearch
### Erstellen Sie den Index 'games'
Stellen Sie sicher, dass Sie Zugriff zu 'curl' Befehlen im Terminal haben und Ihr Webserver gestartet ist.<br>
Ihr Webserver sollte nun über 'localhost:9200' erreichbar sein. Überprüfe dies mit ` curl 'localhost:9200/_cat/health?v `<br>
Die Antwort sollte ungefähr so aussehen:
```
epoch      timestamp cluster       status node.total node.data shards pri relo init unassign
1394735289 14:28:09  elasticsearch green           1         1      0   0    0    0        0
```

führen Sie dann folgenden Befehl aus:
<details>
<summary>Dieser (lange) Befehl initialisiert den 'games'-Index mit den richtigen Mappings und Settings.</summary>

```
curl -X PUT "localhost:9200/games/" -H 'Content-Type: application/json' -d'
{
    "mappings": {
        "properties": {
            "dlc": {
                "type": "integer"
            },
            "id": {
                "type": "long"
            },
            "mc_score": {
                "type": "integer"
            },
            "players": {
                "type": "long"
            },
            "releasedate": {
                "type": "date"
            }
        }
    },
    "settings": {
        "index": {
            "analysis": {
                "filter": {
                    "my_stemmer": {
                        "name": "english",
                        "type": "stemmer"
                    },
                    "mynGram": {
                        "type": "nGram",
                        "min_gram": "4",
                        "max_gram": "5"
                    },
                    "my_stop": {
                            "type": "stop",
                            "stopwords": "_english_"
                        }
                },
                "search_analyzer": {
                    "my_search_analyzer": {
                    "filter": [
                        "standard",
                        "lowercase",
                        "mynGram"
                    ],
                    "type": "custom",
                    "tokenizer": "standard"
                    }
                },
                "index_analyzer": {
                    "my_index_analyzer": {
                    "filter": [
                        "lowercase",
                        "mynGram"
                    ],
                    "type": "custom",
                    "tokenizer": "standard"
                    }
                },
                "analyzer": {
                    "english_stemmer": {
                        "filter": [
                            "lowercase",
                            "my_stemmer"
                        ],
                        "tokenizer": "standard"
                    }
                }
            }
        }
    }
}
```
</details>

### Herunterladen und Formattieren der Daten
#### Möglichkeit 1: Schritt-Für-Schritt Formattieren und Indexieren der Daten // FUNKTIONIERT DERZEIT NICHT, BITTE WENDEN SIE SICH AN MÖGLICHKEIT 2! //:
Stellen Sie sicher, dass Python auf dem aktuellsten Stand ist:<br>
` $ sudo apt-get install python3.7`<br>
Installieren Sie die benötigten Packages:<br>
` $ pip install python-dateutil `<br>
` $ pip install elasticsearch `<br>
Laden Sie sich das Dataset herunter und führen Sie unser Python Script "All-In-One.py" im __gleichen Ordner__ wie das Dataset aus.<br>
Nachdem das Script erfolgreich terminiert, sollten die Einträge in ElasticSearch indexiert sein.

#### Möglichkeit 2: Nur Indexieren der Daten
Stellen Sie sicher, dass Python auf dem aktuellsten Stand ist:<br>
` $ sudo apt-get install python3.7`<br>
Installieren Sie die benötigten Packages:<br>
` $ pip install elasticsearch `<br>
Laden Sie sich [games.json](https://www.dropbox.com/s/8ncbso94odpggjj/games.json) herunter und führen Sie das Python Script "indexer.py" __im gleichen Ordner__ wie das Dataset aus.<br>
Nachdem das Script erfolgreich terminiert, sollten die Einträge in ElasticSearch indexiert sein.


Überprüfen Sie die erfolgreiche Installation mit einer Suche, nach z.B. "Counter-Strike" in der im Browser geöffneten "Index.php" <br>
bzw. mit dem Befehl ` $ curl 'localhost:9200/games/_search?`
