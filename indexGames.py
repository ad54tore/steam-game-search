from elasticsearch import Elasticsearch
import json

es=Elasticsearch()
with open('games.json', 'r') as myfile:
    data = myfile.read()

payload = json.loads(data)

print(payload[1]['id'])

i = 0
txt = "Updated {} documents"
for e in payload:
    es.index(index='games', doc_type='_doc', id=e['id'], body=e)
    print(txt.format(i))
    i = i+1
    


