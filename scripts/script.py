import json;

with open('raw.json', 'r') as myfile:data = myfile.read();

obj=json.loads(data);

test = json.dumps(obj);

def replacing(f):
 	f = f.replace(' "AboutText"','"text"');f = f.replace(' "windows": "False",','"platforms":[');f = f.replace(' "windows": "True",','"platforms":["Windows", '); f = f.replace(' "linux": "False",','');f = f.replace(' "linux": "True",','"Linux", ');f = f.replace(' "mac": "False",','], ');f = f.replace(' "mac": "True",','"Mac"], ');f = f.replace(' "CategorySinglePlayer": "False",','"genres":[');f = f.replace(' "CategorySinglePlayer": "True",','"genres":["SinglePlayer", ');f = f.replace(' "CategoryMultiplayer": "False",','');f = f.replace(' "CategoryMultiplayer": "True",','"Multiplayer", ');f = f.replace(' "CategoryCoop": "False",','');f = f.replace(' "CategoryCoop": "True",','"Coop", ');f = f.replace(' "CategoryMMO": "False",','');f = f.replace(' "CategoryMMO": "True",','"MMO", ');f = f.replace(' "CategoryVRSupport": "False",','');f = f.replace(' "CategoryVRSupport": "True",','"VR", ');f = f.replace(' "GenreIsNonGame": "False",','');f = f.replace(' "GenreIsNonGame": "True",','"NonGame", ');f = f.replace(' "GenreIsIndie": "False",','');f = f.replace(' "GenreIsIndie": "True",',' "Indie", ');f = f.replace(' "GenreIsAction": "False",','');f = f.replace(' "GenreIsAction": "True",','"Action", ');f = f.replace(' "GenreIsAdventure": "False",','');f = f.replace(' "GenreIsAdventure": "True",','"Adventure", ');f = f.replace(' "GenreIsCasual": "False",','');f = f.replace(' "GenreIsCasual": "True",','"Casual", ');f = f.replace(' "GenreIsStrategy": "False",','');f = f.replace(' "GenreIsStrategy": "True",','"Strategy", ');f = f.replace(' "GenreIsRPG": "False",','');f = f.replace(' "GenreIsRPG": "True",','"RPG", ');f = f.replace(' "GenreIsSimulation": "False",','');f = f.replace(' "GenreIsSimulation": "True",','"Simulation", ');f = f.replace(' "GenreIsEarlyAccess": "False",','');f = f.replace(' "GenreIsEarlyAccess": "True",','"EarlyAccess", ');f = f.replace(' "GenreIsFreeToPlay": "False",','');f = f.replace(' "GenreIsFreeToPlay": "True",','"FreeToPlay", ');f = f.replace(' "GenreIsSports": "False",','');f = f.replace(' "GenreIsSports": "True",','"Sports", ');f = f.replace(' "GenreIsRacing": "False",','');f = f.replace(' "GenreIsRacing": "True",','"Racing", ');f = f.replace(' "GenreIsMassivelyMultiplayer": "False",','], ');f = f.replace(' "GenreIsMassivelyMultiplayer": "True",',' "MassivelyMultiplayer"], ');f = f.replace(', ]',']');f = f.replace('? ','?');f = f.replace('?','? ');f = f.replace('! ','!');f = f.replace('!','! ');f = f.replace(', ',',');f = f.replace(',',', ');f = f.replace('. ','.');f = f.replace('.','. ');return f;

out = replacing(test);

text_file = open("output.txt", "w");
text_file.write(out);
text_file.close();